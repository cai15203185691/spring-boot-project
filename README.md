# spring-boot-project

#### 介绍
​	基于SpringBoot2.6.4与各个框架技术整合, 项目持续更新中。后续涉及完整项目配置等信息。。。

#### 软件架构
​	SpringBoot与各个框架技术整合


#### 安装教程

1.  下载项目到本地
2.  导入DEA引入相关依赖
3.  根据自己需要查看相关项目

#### 使用说明
##### 1.  spring-boot-authorization 		安全框架

######    	1.1.    spring-boot-security-page            		 	springboot整合security
######    	1.2.    spring-boot-shiro-page         				    springboot整合shiro
##### 2.  spring-boot-data 		            数据库相关

######    	2.1.    spring-boot-jpa            		 			    springboot整合hibernate
######    	2.2.    spring-boot-mybatis         				    springboot整合mybatis
######    	2.3.    spring-boot-mybatis-plus    			        springboot整合mybatis-plus
##### 3.  spring-boot-docs 		            Restful API 文档

######    	3.1.    spring-boot-smart-doc            		        springboot整合smart doc
###### 	    3.2.    spring-boot-spring--restdocs     	            springboot整合spring Restdocs
######    	3.3.    spring-boot-swagger             	  	        springboot整合swagger
##### 4.  spring-boot-web  		            springboot整合页面

######    	4.1.    spring-boot-freemarker    			            springboot整合freemarker
######    	4.2.    spring-boot-thymeleaf     			            springboot整合thymeleaf
