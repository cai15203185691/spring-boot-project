package com.zhou.project.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-04 11:02
 * @description: TODO
 */
@Configuration
@EnableOpenApi
public class SwaggerConfiguration {


    

}
