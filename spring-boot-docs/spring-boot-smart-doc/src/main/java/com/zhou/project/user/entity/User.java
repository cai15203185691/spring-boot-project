package com.zhou.project.user.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:38
 * @description: [用户实体对象]
 */
@Getter
@Setter
@ToString
public class User {
    /**
     * 用户主键
     * @mock 1
     * @since 1.0.0
     */
    private int id;
    /**
     * 用户姓名
     * @mock 汪馨
     * @since 1.0.0
     * @required
     */
    private String name;
    /**
     * 用户性别
     * @mock 1:男 2:女
     * @since 1.0.0
     * @required
     */
    private int gender;
    /**
     * 出生日期
     * @mock 2020-02-20
     * @since 1.0.0
     * @required
     */
    private Date birthday;
    /**
     * 家庭住址
     * @mock 北京市海淀区
     * @since 1.1.0
     */
    private String address;
}
