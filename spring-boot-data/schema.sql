/*
 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Schema         : zhou

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001
*/

-- ----------------------------
-- 单表 用户表 users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户主键',
  `name` varchar(35) NOT NULL COMMENT '用户名称',
  `gender` int(1) NOT NULL COMMENT '用户性别 1:男 2:女',
  `birthday` date NOT NULL COMMENT '出生日期',
  `address` varchar(255) NULL DEFAULT NULL COMMENT '家庭住址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- 关系表 部门表 department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
   `department_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门主键',
   `department_name` varchar(255) NOT NULL COMMENT '部门名称',
   `department_location` varchar(255) NULL DEFAULT NULL COMMENT '部门位置',
   `created_time` timestamp NULL DEFAULT NULL COMMENT '插入时间',
   `updated_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
   PRIMARY KEY (`department_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;

-- ----------------------------
-- 关系表 员工表 employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees`  (
  `employees_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工主键',
  `employees_name` varchar(255) NOT NULL COMMENT '员工名称',
  `employees_jobs` varchar(255) NULL DEFAULT NULL COMMENT '员工岗位',
  `employees_salary` int(8) NULL DEFAULT NULL COMMENT '员工薪资',
  `created_time` timestamp NULL DEFAULT NULL COMMENT '插入时间',
  `updated_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门外键',
  PRIMARY KEY (`employees_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ROW_FORMAT = Dynamic;
