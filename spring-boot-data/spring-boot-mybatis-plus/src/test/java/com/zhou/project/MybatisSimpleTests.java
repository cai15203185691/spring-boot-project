package com.zhou.project;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhou.project.simple.entity.Users;
import com.zhou.project.simple.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 10:59
 * @description: Mybatis简单CRUD单元测试
 */
@SpringBootTest
public class MybatisSimpleTests {

    @Autowired
    private UsersService usersService;

    /**
     * 测试 添加 人员
     */
    @Test
    public void insert()
    {
        Users entity = new Users();
        entity.setName("小李子");
        entity.setGender(1);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区");

        assertEquals("添加人员测试:", true, usersService.save(entity));
    }

    /**
     * 测试 修改 人员
     */
    @Test
    public void update()
    {
        Users entity = new Users();
        entity.setId(1);
        entity.setName("小李子1");
        entity.setGender(2);
        entity.setBirthday(new Date());
        entity.setAddress("北京市海淀区1");

        assertEquals("修改人员测试:", true, usersService.updateById(entity));
    }

    /**
     * 测试 删除 人员
     */
    @Test
    public void delete()
    {
        int id = 1;
        assertEquals("删除人员测试:", true, usersService.removeById(id));
    }

    /**
     * 测试 批量删除 人员
     */
    @Test
    public void deletes()
    {
        Collection<Integer> ids = List.of(1, 2, 3);
        assertEquals("批量删除人员测试:", true, usersService.removeBatchByIds(ids));
    }

    /**
     * 测试 主键 查询 人员
     */
    @Test
    public void load()
    {
        Users entity = usersService.getById(4);
        assertNotNull("主键查询人员测试:", entity);
    }

    /**
     * 测试 分页 查询
     */
    @Test
    public void page()
    {
        Page<Users> page = usersService.page(1, 10);
        System.out.println("数据:"+page.getRecords());
        System.out.println("当前页:"+page.getCurrent());
        System.out.println("每页数:"+page.getSize());
        System.out.println("总页数:"+page.getPages());
        System.out.println("总条数:"+page.getTotal());
    }

    /**
     * 测试 条件 查询
     */
    @Test
    public void condition()
    {
        Users entity = new Users();
        entity.setName("小");

        List<Users> list = usersService.condition(entity);
        list.forEach(System.out::println);
    }

}
