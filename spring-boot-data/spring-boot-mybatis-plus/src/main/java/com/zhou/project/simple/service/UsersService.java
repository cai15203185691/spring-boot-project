package com.zhou.project.simple.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.project.simple.dao.UsersDao;
import com.zhou.project.simple.entity.Users;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:51
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class UsersService extends ServiceImpl<UsersDao, Users> {

    /**
     * 分页查询
     * @param current 当前页
     * @param sizes 每页展示数
     * @return 分页对象
     */
    public Page<Users> page(int current, int sizes)
    {
        Page<Users> pages = Page.of(current, sizes);
        return baseMapper.selectPage(pages, null);
    }

    /**
     * 条件查询
     * @param entity 查询条件
     * @return 符合条件结果集
     */
    public List<Users> condition(Users entity)
    {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        //名称模糊查询
        if(StringUtils.hasLength(entity.getName()))
        {
            wrapper.like("name", entity.getName());
        }
        return baseMapper.selectList(wrapper);
    }

}
