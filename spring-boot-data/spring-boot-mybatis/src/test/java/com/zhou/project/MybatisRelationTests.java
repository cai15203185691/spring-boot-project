package com.zhou.project;
import java.util.ArrayList;
import com.zhou.project.relation.entity.Department;

import com.zhou.project.relation.entity.Employees;
import com.zhou.project.relation.service.DepartmentService;
import com.zhou.project.relation.service.EmployeesService;
import com.zhou.project.simple.entity.Users;
import com.zhou.project.simple.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 10:59
 * @description: Mybatis对象关系单元测试
 */
@SpringBootTest
public class MybatisRelationTests {

    //注入员工逻辑层
    @Autowired
    private EmployeesService employeesService;

    //注入部门逻辑层
    @Autowired
    private DepartmentService departmentService;

    /**
     * 添加员工部门并建立关系
     */
    @Test
    public void insertEmployees()
    {
        //1.创建部门对象
        Department department = new Department();
        department.setDepartmentName("软件研发一部");
        department.setDepartmentLocation("北京市中关村软件园一期");

        //2.创建员工对象
        Employees employees = new Employees();
        employees.setEmployeesName("小王");
        employees.setEmployeesJobs("软件工程师");
        employees.setEmployeesSalary("8000");

        //3.建立员工部门关系
        employees.setDepartment(department);

        //4.执行数据库插入
        employeesService.insert(employees);
    }

    /**
     * 查询员工并关联部门
     */
    @Test
    public void loadEmployees()
    {
        Employees employees = employeesService.load(1);
        System.out.println("员工:"+employees);
    }

    /**
     * 查询部门并关联员工
     */
    @Test
    public void loadDepartment()
    {
        Department department = departmentService.loadDepartment(6);
        System.out.println("部门:"+department);
    }

}
