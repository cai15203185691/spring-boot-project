package com.zhou.project.relation.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:12
 * @description: [员工实体对象]
 */
@Getter
@Setter
@ToString
public class Employees {
    /**
     * 员工主键
     */
    private int employeesId;
    /**
     * 员工名称
     */
    private String employeesName;
    /**
     * 员工岗位
     */
    private String employeesJobs;
    /**
     * 员工薪资
     */
    private String employeesSalary;
    /**
     * 员工所属部门
     */
    private Department department;
}
