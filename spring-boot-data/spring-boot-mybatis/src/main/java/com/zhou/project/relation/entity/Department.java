package com.zhou.project.relation.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:13
 * @description: [部门实体对象]
 */
@Getter
@Setter
@ToString
public class Department {
    /**
     * 部门主键
     */
    private int departmentId;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 部门位置
     */
    private String departmentLocation;
    /**
     * 部门下属员工
     */
    private List<Employees> employeesList;
}
