package com.zhou.project.simple.service;

import com.zhou.project.simple.dao.UsersDao;
import com.zhou.project.simple.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:51
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    /**
     * 插入用户信息
     * @param entity 用户对象
     * @return 影响条数
     */
    @Transactional
    public int insert(Users entity)
    {
        return usersDao.insert(entity);
    }

    /**
     * 修改用户信息
     * @param entity 用户对象
     * @return 影响条数
     */
    @Transactional
    public int update(Users entity)
    {
        return usersDao.update(entity);
    }

    /**
     * 删除用户信息
     * @param id 用户主键
     * @return 影响条数
     */
    @Transactional
    public int delete(int id)
    {
        return usersDao.delete(id);
    }

    /**
     * 删除用户组
     * @param ids 用户主键组
     * @return 影响条数
     */
    @Transactional
    public int deletes(int... ids)
    {
        return usersDao.deletes(ids);
    }

    /**
     * 主键查询用户
     * @param id 用户主键
     * @return 用户对象
     */
    public Users queryId(int id)
    {
        return usersDao.queryId(id);
    }

    /**
     * 条件查询总数
     * @param params 动态条件
     * @return 总条数
     */
    public long queryCount(Map<String, Object> params)
    {
        return usersDao.queryCount(params);
    }

    /**
     * 条件查询集合
     * @param params 动态条件
     * @return 结果集
     */
    public List<Users> queryPage(Map<String, Object> params)
    {
        return usersDao.queryPage(params);
    }

}
