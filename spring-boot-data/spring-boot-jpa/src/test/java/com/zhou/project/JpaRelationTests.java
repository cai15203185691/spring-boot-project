package com.zhou.project;

import com.zhou.project.relation.entity.Department;
import com.zhou.project.relation.entity.Employees;
import com.zhou.project.relation.service.DepartmentService;
import com.zhou.project.relation.service.EmployeesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 10:59
 * @description: JPA对象关系单元测试
 */
@SpringBootTest
public class JpaRelationTests {

    //注入员工逻辑层
    @Autowired
    private EmployeesService employeesService;

    //注入部门逻辑层
    @Autowired
    private DepartmentService departmentService;

    /**
     * 添加员工部门并建立关系
     */
    @Test
    public void insertEmployees()
    {
        //1.创建部门对象
        Department department = new Department();
        department.setDepartmentName("软件研发一部");
        department.setDepartmentLocation("北京市中关村软件园一期");

        //2.创建员工对象
        Employees employees = new Employees();
        employees.setEmployeesName("小王");
        employees.setEmployeesJobs("软件工程师");
        employees.setEmployeesSalary("8000");

        //3.建立员工部门关系
        employees.setDepartment(department);

        //4.执行数据库插入
        employeesService.save(employees);
    }

    /**
     * 主键ID查询员工
     */
    @Test
    public void loadEmployees()
    {
        Employees entity = employeesService.load(15);
        System.out.println( entity );
    }

    /**
     * 添加员工部门并建立关系
     */
    @Test
    public void insertDepartment()
    {
        //1.创建部门对象
        Department department = new Department();
        department.setDepartmentName("软件研发一部");
        department.setDepartmentLocation("北京市中关村软件园一期");

        //2.创建员工对象
        List<Employees> employeesList = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            Employees employees = new Employees();
            employees.setEmployeesName("小王"+i);
            employees.setEmployeesJobs("软件工程师"+i);
            employees.setEmployeesSalary("8000"+i);

            //3.建立员工部门关系
            employees.setDepartment(department);

            employeesList.add(employees);
        }

        //4.建立部门员工关系
        department.setEmployeesList(employeesList);

        //5.执行数据库插入
        departmentService.save(department);
    }

    /**
     * 主键ID查询部门
     */
    @Test
    public void loadDepartment()
    {
        Department entity = departmentService.load(8);
        System.out.println( entity );
    }

}
