package com.zhou.project.simple.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:38
 * @description: [用户实体对象]
 */
@Data
@Entity
@Table(name = "users", catalog = "zhou")
public class Users {
    /** 用户主键 */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /** 用户姓名 */
    @Column(nullable = false)
    private String name;

    /** * 用户性别  1:男 2:女  */
    @Column(nullable = false)
    private int gender;

    /** 出生日期 */
    @Column(nullable = false)
    private Date birthday;

    /** 家庭住址 */
    @Column
    private String address;
}
