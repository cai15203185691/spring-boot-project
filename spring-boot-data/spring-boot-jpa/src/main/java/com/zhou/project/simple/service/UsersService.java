package com.zhou.project.simple.service;

import com.zhou.project.simple.dao.UsersDao;
import com.zhou.project.simple.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-26 16:51
 * @description: TODO
 */
@Service
@Transactional(readOnly = true)
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    /**
     * 插入|修改用户信息
     * @param entity 用户对象
     */
    @Transactional
    public void saveOrUpdate(Users entity)
    {
        usersDao.save(entity);
    }

    /**
     * 删除用户信息
     * @param id 用户主键
     */
    @Transactional
    public void delete(int id)
    {
        usersDao.deleteById(id);
    }

    /**
     * 删除用户组
     * @param ids 用户主键组
     */
    @Transactional
    public void deletes(Collection<Integer> ids)
    {
        usersDao.deleteAllById(ids);
    }

    /**
     * 主键查询用户
     * @param id 用户主键
     * @return 用户对象
     */
    public Users queryId(int id)
    {
        return usersDao.findById(id).get();
    }

    /**
     * 条件查询集合
     * @param current 页码从0开始
     * @param sizes 每页展示数据量
     * @return 结果集
     */
    public Page<Users> queryPage(int current, int sizes)
    {
        PageRequest page = PageRequest.of(current, sizes);
        return usersDao.findAll(page);
    }

}
