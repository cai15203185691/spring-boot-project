package com.zhou.project.relation.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-02-28 12:13
 * @description: [部门实体对象]
 */
@Getter
@Setter
@Entity
@Table(name = "department")
@EntityListeners(value = AuditingEntityListener.class)
public class Department {
    /**
     * 部门主键
     */
    @Id
    @Column(name = "department_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int departmentId;
    /**
     * 部门名称
     */
    @Column(name = "department_name", nullable = false)
    private String departmentName;
    /**
     * 部门位置
     */
    @Column(name = "department_location")
    private String departmentLocation;
    /**
     * 添加日期 自动更新
     */
    @CreatedDate
    @Column(name = "created_time", nullable = false, updatable = false)
    private Timestamp createdTime;
    /**
     * 修改日期 自动更新
     */
    @LastModifiedDate
    @Column(name = "updated_time")
    private Timestamp updatedTime;
    /**
     * 部门下属员工
     */
    @OneToMany(
        fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        targetEntity = Employees.class
    )
    @JoinColumn(name = "department_id")
    private List<Employees> employeesList;
}
