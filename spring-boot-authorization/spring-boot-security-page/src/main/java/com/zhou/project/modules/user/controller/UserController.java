package com.zhou.project.modules.user.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhouhonggang
 * @version 1.0.0
 * @project spring-boot-project
 * @datetime 2022-03-07 16:12
 * @description: TODO
 */
@Controller
@RequestMapping("user")
public class UserController {

    @PreAuthorize("hasRole('USER_INSERT')")
    @RequestMapping("insert")
    public String insert()
    {
        return "user/insert";
    }

}
