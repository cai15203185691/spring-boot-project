/*
 Source Server Type    : MySQL
 Target Server Type    : MySQL
*/
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_authority 权限表
-- ----------------------------
DROP TABLE IF EXISTS `system_authority`;
CREATE TABLE `system_authority`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '权限名称',
  `code` varchar(60) NOT NULL COMMENT '权限编码',
  `uri` varchar(60) NULL DEFAULT NULL COMMENT '访问路径',
  `revision` int(11) NULL DEFAULT NULL COMMENT '乐观锁',
  `delete_flag` int(11) NULL DEFAULT NULL COMMENT '删除状态',
  `created_by` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int(11) NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 COMMENT = '系统管理_权限管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_role 角色表
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '角色名称',
  `code` varchar(60) NOT NULL COMMENT '角色编号',
  `revision` int(11) NULL DEFAULT NULL COMMENT '乐观锁',
  `delete_flag` int(11) NULL DEFAULT NULL COMMENT '删除状态',
  `created_by` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int(11) NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 COMMENT = '系统管理_角色管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_role_authority 角色权限中间表
-- ----------------------------
DROP TABLE IF EXISTS `system_role_authority`;
CREATE TABLE `system_role_authority`  (
  `role_id` int(32) NOT NULL COMMENT '角色外键',
  `authority_id` int(32) NOT NULL COMMENT '权限外键',
  PRIMARY KEY (`role_id`, `authority_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '角色权限关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_user 用户表
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(60) NOT NULL COMMENT '登陆账号',
  `pass` varchar(60) NOT NULL COMMENT '登陆密码',
  `gender` int(11) NULL DEFAULT NULL COMMENT '用户性别',
  `nick` varchar(60) NULL DEFAULT NULL COMMENT '用户昵称',
  `phone` varchar(11) NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(30) NULL DEFAULT NULL COMMENT '电子邮箱',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `address` varchar(60) NULL DEFAULT NULL COMMENT '家庭住址',
  `remarks` varchar(900) NULL DEFAULT NULL COMMENT '个人介绍',
  `locked` int(11) NULL DEFAULT NULL COMMENT '是否锁定',
  `revision` int(11) NULL DEFAULT NULL COMMENT '乐观锁',
  `delete_flag` int(11) NULL DEFAULT NULL COMMENT '删除状态',
  `created_by` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` int(11) NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 COMMENT = '系统管理_用户管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_user_role 用户角色中间表
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role`  (
  `user_id` int(32) NOT NULL COMMENT '用户外键',
  `role_id` int(32) NOT NULL COMMENT '角色外键',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB COMMENT = '用户角色关系' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
